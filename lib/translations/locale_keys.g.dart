// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const contact_us_title = 'contact_us_title';
  static const contact_us_info = 'contact_us_info';
  static const login_screen_member_login = 'login_screen_member_login';
  static const login_screen_already_loggedin_in_different_device = 'login_screen_already_loggedin_in_different_device';
  static const login_screen_invalid_session = 'login_screen_invalid_session';
  static const login_screen_logged_in = 'login_screen_logged_in';
  static const login_screen_fill_the_parts = 'login_screen_fill_the_parts';
  static const login_screen_login_button = 'login_screen_login_button';
  static const login_screen_password_hint_text = 'login_screen_password_hint_text';
  static const login_screen_email_hint_text = 'login_screen_email_hint_text';
  static const login_screen_email_wrong_form = 'login_screen_email_wrong_form';
  static const tabs_screen_menu_home = 'tabs_screen_menu_home';
  static const tabs_screen_analyzes = 'tabs_screen_analyzes';
  static const tabs_screen_prices = 'tabs_screen_prices';
  static const tabs_screen_search = 'tabs_screen_search';
  static const tabs_screen_settings = 'tabs_screen_settings';

}
