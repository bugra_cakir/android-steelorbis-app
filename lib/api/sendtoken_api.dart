import 'package:http/http.dart' as http;

//parameters for link: token, lang, apptype

Future<void> sendTokenService(String firebaseToken) async {
  var sendTokenResponse = await http.get(
    Uri.parse(
        'https://www.steelorbis.com/ws/sendtoken/?token=$firebaseToken&lang=eng&appType=3'),
  );

  var link =
      'https://www.steelorbis.com/ws/sendtoken/?token=${firebaseToken}&lang=eng&appType=3';
  print('The firebase sendtoken link is: ${link}');
}
