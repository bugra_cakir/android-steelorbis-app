import 'package:flutter/material.dart';
import 'package:snippet_coder_utils/FormHelper.dart';
import 'package:snippet_coder_utils/ProgressHUD.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final List<Color> LoginPageColor = [
    const Color(0xccbfbbbb),
    const Color(0xff424242),
  ];
  bool isAPIcallProcess = false;
  bool hidePassword = true;
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();
  String? username;
  String? password;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xffb05f3c),
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          bottomOpacity: 0.0,
          elevation: 0,
          title: Center(
            child: Image.asset(
              'assets/icons/logo_buyuk.png',
              height: 150,
              width: 180,
            ),
          ),
          backgroundColor: Colors.transparent,
        ),
        body: ProgressHUD(
          child: Form(
            key: globalFormKey,
            child: _loginUI(context),
          ),
          inAsyncCall: isAPIcallProcess,
          opacity: 0.3,
          key: UniqueKey(),
        ),
      ),
    );
  }

  Widget _loginUI(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height / 2.2,
            decoration: BoxDecoration(
              // image: DecorationImage(
              //   colorFilter: new ColorFilter.mode(
              //       Colors.transparent.withOpacity(0.4), BlendMode.dstATop),
              //   image: AssetImage("assets/icons/so_orange_tr.png"),
              //   fit: BoxFit.cover,
              // ),
              gradient: LinearGradient(
                  colors: LoginPageColor,
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(100),
                bottomRight: Radius.circular(100),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Image.asset(
                    "assets/icons/so_orange_tr.png",
                    width: 250,
                    fit: BoxFit.contain,
                    color: const Color.fromRGBO(225, 180, 150, 0.7),
                    colorBlendMode: BlendMode.modulate,
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, bottom: 30, top: 50),
            child: Text(
              'Login',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24,
                  color: Colors.white),
            ),
          ),
          FormHelper.inputFieldWidget(
            context,
            const Icon(Icons.person),
            'username',
            'Email',
            (onValidateVal) {
              if (onValidateVal.isEmpty) {
                return 'Username can not be empty.';
              }
              return null;
            },
            (onSavedVal) {
              username = onSavedVal;
            },
            borderFocusColor: Colors.deepOrange,
            prefixIconColor: Colors.white,
            borderColor: Colors.white,
            textColor: Colors.white,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: FormHelper.inputFieldWidget(
              context,
              const Icon(Icons.lock),
              'password',
              'Şifre',
              (onValidateVal) {
                if (onValidateVal.isEmpty) {
                  return 'Şifre boş olamaz.';
                }
                return null;
              },
              (onSavedVal) {
                password = onSavedVal;
              },
              borderFocusColor: Colors.deepOrange,
              prefixIconColor: Colors.white,
              borderColor: Colors.white,
              textColor: Colors.white,
              obscureText: hidePassword,
              suffixIcon: IconButton(
                onPressed: () {
                  setState(() {
                    hidePassword = !hidePassword;
                  });
                },
                color: Colors.white,
                icon: Icon(
                  hidePassword ? Icons.visibility : Icons.visibility_off,
                ),
              ),
            ),
          ),
          SizedBox(height: 20),
          Center(
            child: FormHelper.submitButton('Giriş', () {},
                btnColor: Colors.red,
                borderColor: Colors.white,
                txtColor: Colors.white),
          )
        ],
      ),
    );
  }
}
