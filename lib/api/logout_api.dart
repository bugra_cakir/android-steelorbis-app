import 'package:http/http.dart' as http;

//parameters: username, apptype

Future<void> logoutService(String username) async {
  var logoutResponse = await http.get(
    Uri.parse(
        'https://www.steelorbis.com/ws/logout/?username=${username}&appType=3'),
  );
}
