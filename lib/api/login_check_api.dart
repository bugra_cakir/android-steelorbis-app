import 'dart:convert';

import 'package:http/http.dart' as http;

Future<bool?> checkLoginService(String username, String ticket) async {
  var loginCheckJsonData = null;
  bool trueOrFalse;
  var loginCheckResponse = await http.get(
    Uri.parse(
        'http://www.steelorbis.com/ws/logincheck/?username=${username}&ticket=${ticket}&appType=3'),
  );
  if (loginCheckResponse.statusCode == 200) {
    loginCheckJsonData = json.decode(loginCheckResponse.body);
    print('login Check Json DATA is: ${loginCheckJsonData}');
    if (loginCheckJsonData == true) {
      trueOrFalse = true;
    } else {
      trueOrFalse = false;
    }
    print('login Check Json DATA bool is:${trueOrFalse.toString()}');
    return trueOrFalse;
  }

  var link =
      'http://www.steelorbis.com/ws/logincheck/?username=${username}&ticket=${ticket}&appType=3';
  print('The check login link is: ${link}');
}
//https://www.steelorbis.com/ws/logincheck/?username=cakirbugra99@gmail.com&ticket=D7S5E8F1C9C8&appType=3
