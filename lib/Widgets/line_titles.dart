// ignore_for_file: file_names

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';

class LineTitles {
  static getTitleData() {
    return FlTitlesData(
      topTitles: SideTitles(
        showTitles: false,
      ),
      rightTitles: SideTitles(
        showTitles: false,
      ),
      show: true,
      bottomTitles: SideTitles(
        interval: 1,
        showTitles: true,
        rotateAngle: 60.0,
        reservedSize: 24, //bottom space
        getTextStyles: (context, value) => TextStyle(
          color: Color(0xff68737d),
          fontWeight: FontWeight.bold,
          fontSize: 11,
        ),
        margin: 8,

        getTitles: (value) {
          switch (value.toInt()) {
            case 1:
              return 'W2';
            case 3:
              return 'W4';
            case 5:
              return 'W6';
            case 7:
              return 'W8';
            case 9:
              return 'W10';
            case 11:
              return 'W12';
            case 13:
              return 'W14';
            case 15:
              return 'W16';
            case 17:
              return 'W18';
            case 19:
              return 'W20';
            case 21:
              return 'W22';
            case 23:
              return 'W24';
            case 25:
              return 'W26';
            case 27:
              return 'W28';
            case 29:
              return 'W30';
            case 31:
              return 'W32';
            case 33:
              return 'W34';
            case 35:
              return 'W36';
            case 37:
              return 'W38';
            case 39:
              return 'W40';
            case 41:
              return 'W42';
            case 43:
              return 'W44';
            case 45:
              return 'W46';
            case 47:
              return 'W48';
            case 49:
              return 'W50';
            case 51:
              return 'W52';
          }
          return '';
        },
      ),
      leftTitles: SideTitles(
        showTitles: true,
        rotateAngle: -45.0,
        interval: 1,
        getTextStyles: (context, value) => const TextStyle(
          color: Color(0xff67727d),
          fontWeight: FontWeight.bold,
          fontSize: 12,
        ),
        getTitles: (value) {
          switch (value.toInt()) {
            case 0:
              return '0';
            case 2:
              return '200';
            case 4:
              return '400';
            case 6:
              return '600';
          }
          return '';
        },
        reservedSize: 25,
      ),
    );
  }
}
