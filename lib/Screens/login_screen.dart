import 'dart:convert';
import 'dart:core';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:easy_localization/src/public_ext.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:steelorbis_android_app/Screens/tabs_screen.dart';
import 'package:steelorbis_android_app/Widgets/loading_animation.dart';
import 'package:steelorbis_android_app/translations/locale_keys.g.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var emailController = TextEditingController();
  var passController = TextEditingController();
  late final FirebaseMessaging _firebaseMessaging;
  bool _isLoading = false;

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    emailController.dispose();
    passController.dispose();
    super.dispose();
  }

  final List<Color> LoginPageColor = [
    const Color(0xffffffff),
    const Color(0xffe0e0e0),
  ];
  bool flag = true;
  bool rememberMe = false;
  bool isApiCallProcess = false;

  final scaffoldKey = GlobalKey<ScaffoldState>();
  // final <FormState> _globalFormKey = GlobalKey<FormState>();
  final _formKey = GlobalKey<FormState>();
  bool hidePassword = true;

  Widget buildEmail() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 6,
                offset: Offset(0, 2),
              ),
            ],
          ),
          height: 60,
          child: TextFormField(
            keyboardType: TextInputType.emailAddress,
            controller: emailController,
            cursorColor: Colors.deepOrange,
            validator: EmailValidator(
                errorText:
                    //mail hatalı çeviri
                    LocaleKeys.login_screen_email_wrong_form.tr()),

            //   // return value.contains(
            //   //         RegExp(r'[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}'))
            //   //     ? null
            //   //     : "Invalid E-mail";

            decoration: InputDecoration(
              //email çeviri
              hintText: LocaleKeys.login_screen_email_hint_text.tr(),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: Theme.of(context).accentColor.withOpacity(0.2),
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(9)),
                borderSide:
                    BorderSide(width: 3, color: Colors.deepOrangeAccent),
              ),
              prefixIcon: const Icon(
                Icons.email_rounded,
                color: const Color(0xff636262),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget buildPassword() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 10,
        ),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: const [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 6,
                offset: Offset(0, 2),
              ),
            ],
          ),
          height: 60,
          child: TextFormField(
            controller: passController,
            keyboardType: TextInputType.text,
            cursorColor: Colors.deepOrange,
            obscureText: hidePassword,
            decoration: InputDecoration(
              hintText:
                  //şifre çeviri
                  LocaleKeys.login_screen_password_hint_text.tr(),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context).accentColor.withOpacity(0.2))),
              focusedBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(9)),
                borderSide:
                    BorderSide(width: 3, color: Colors.deepOrangeAccent),
              ),
              prefixIcon: const Icon(
                Icons.lock,
                color: Color(0xff636262),
              ),
              suffixIcon: IconButton(
                onPressed: () {
                  setState(() {
                    hidePassword = !hidePassword;
                  });
                },
                color: Colors.grey.withOpacity(0.7),
                icon: Icon(
                    hidePassword ? Icons.visibility_off : Icons.visibility),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget buildLoginButton() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 12),
      width: double.infinity,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            shadowColor: Colors.deepOrange,
            primary: Colors.deepOrangeAccent,
            elevation: 8),
        child: Text(
          //Log in butonu
          LocaleKeys.login_screen_login_button,
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),
        ).tr(),
        onPressed: () {
          if (!_formKey.currentState!.validate()) {
            return;
          }
          setState(() {
            _isLoading = true;
          });
          login();
        },
      ),
    );
  }

  Widget contactUsTileWidget() {
    return ExpansionTile(
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              LocaleKeys.contact_us_info,
              // 'If you are having problems with logging into the application, you can contact us by e-mail or telephone. While we offer limited number of services in the mobile application, you can visit our website to view all our services.',
              style: TextStyle(fontSize: 18, letterSpacing: 0.2),
            ).tr(),
            const SizedBox(height: 18),
            Row(
              children: [
                Icon(
                  FontAwesomeIcons.phoneVolume,
                  color: Colors.deepOrange,
                ),
                const SizedBox(width: 8),
                Text(
                  '+90(216)589 60 49',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            const SizedBox(height: 12),
            Row(
              children: [
                Icon(
                  FontAwesomeIcons.envelope,
                  color: Colors.deepOrange,
                ),
                const SizedBox(width: 8),
                Text(
                  'cs@steelorbis.com',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                )
              ],
            ),
            const SizedBox(height: 12),
          ],
        ),
        const SizedBox(height: 8),
      ],
      leading: Icon(
        FontAwesomeIcons.tty,
        color: Colors.black38,
      ),
      title: Text(
        LocaleKeys.contact_us_title.tr(),
        style: TextStyle(
            color: Colors.deepOrange,
            fontSize: 22,
            fontWeight: FontWeight.bold),
      ),
    );
  }

  //*/*/*/*************OVERRİDE*****************************/*/*/*/*/*/*

  @override
  Widget build(BuildContext context) {
    return _uiSetup(context);
  }

  @override
  Widget _uiSetup(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Center(
          child: Image.asset(
            'assets/icons/logo_buyuk.png',
            height: 150,
            width: 180,
          ),
        ),
        backgroundColor: Colors.black54,
        shadowColor: Colors.grey,
      ),
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: GestureDetector(
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                decoration: BoxDecoration(
                  // SteelOrbis Logo on the background removed
                  // image: DecorationImage(
                  //   colorFilter: new ColorFilter.mode(
                  //       Colors.white.withOpacity(1), BlendMode.dst),
                  //   image: AssetImage("assets/icons/so_orange_tr.png"),
                  //   fit: BoxFit.cover,
                  // ),
                  gradient: LinearGradient(
                      colors: LoginPageColor,
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter),
                ),
                child: _isLoading
                    ? Center(child: LoadingAnimation())
                    : SingleChildScrollView(
                        physics: AlwaysScrollableScrollPhysics(),
                        padding:
                            EdgeInsets.symmetric(horizontal: 25, vertical: 150),
                        child: Form(
                          key: _formKey,
                          autovalidateMode: AutovalidateMode.always,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Row(
                                children: [
                                  const Icon(
                                    FontAwesomeIcons.signInAlt,
                                    color: Colors.black38,
                                  ),
                                  const SizedBox(width: 12),
                                  Text(
                                    //üye Girişi Text
                                    LocaleKeys.login_screen_member_login,
                                    style: TextStyle(
                                      color: Colors.deepOrangeAccent,
                                      fontSize: 24,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ).tr(),
                                ],
                              ),
                              const SizedBox(height: 10),
                              buildEmail(),
                              buildPassword(),
                              const SizedBox(height: 5),
                              buildLoginButton(),
                              const SizedBox(
                                height: 8,
                              ),
                              contactUsTileWidget(),
                            ],
                          ),
                        ),
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<String?> _getId() async {
    var deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      var iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      var androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }

  // Future<String?> _getTokenFromFirebase() async {
  //   _firebaseMessaging = FirebaseMessaging.instance;
  //
  //   var token = await _firebaseMessaging.getToken();
  //   //print("Instance ID: $token");
  //   return token;
  // }

  String http2 = 'https://reqres.in/api/login';

  //Create Function to call LOGIN POST API
  Future<void> login() async {
    String? deviceId = await _getId();
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    String deviceInfo = '${emailController.text}${deviceId}';
    String? firebaseToken = sharedPreferences.getString('FirebaseToken');
    print('firebase token at login is a63nSc : $firebaseToken');

    if (passController.text.isNotEmpty && emailController.text.isNotEmpty) {
      var response = await http.get(
        Uri.parse(
            'https://www.steelorbis.com/ws/login/?username=${emailController.text}&password=${passController.text}&deviceInfo=${deviceInfo}&token=${firebaseToken}&appType=3'),
      );
      var username = emailController.text;
      print(username);
      var link =
          'https://www.steelorbis.com/ws/login/?username=${emailController.text}&password=${passController.text}&deviceInfo=${deviceInfo}&token=${firebaseToken}&appType=3';
      print('The login full link is: ${link}');
      print('username: ${emailController.text}');
      print('password: ${passController.text}');
      print('deviceInfo : $deviceInfo');
      var jsonData = null;

      if (response.statusCode == 200) {
        jsonData = json.decode(response.body);
        print('Response status code: ${response.statusCode}');
        print('Json Data is : ${jsonData}');
        if (jsonData['ticket'] == null) {
          if (jsonData['state'] == 9) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                duration: Duration(seconds: 4),
                backgroundColor: Colors.red,
                content: Text(
                        //Oturum farklı cihazda açık text
                        LocaleKeys
                            .login_screen_already_loggedin_in_different_device)
                    .tr(),
              ),
            );
          } else {
            ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(backgroundColor: Colors.red, content: Text(
                    //Geçersiz mail text
                    LocaleKeys.login_screen_invalid_session).tr()));
          }

          setState(() {
            _isLoading = false;
          });
        } else if (jsonData != null) {
          setState(() {
            _isLoading = false;
          });
          sharedPreferences.setString("token", jsonData['ticket']);
          sharedPreferences.setString("email", jsonData['username']);
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (BuildContext ctx) => TabsScreen(
                username: emailController.text,
              ),
            ),
          );
          print('jsondata Ticket is : ${jsonData['ticket']}');
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              duration: Duration(seconds: 2),
              backgroundColor: Colors.green,
              content: Text(
                  //giriş yapıldı
                  LocaleKeys.login_screen_logged_in).tr(),
            ),
          );

          print('LOGGED İN');
        }
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            duration: Duration(seconds: 3),
            backgroundColor: Colors.red,
            content: Text(
                //Geçersiz mail text
                LocaleKeys.login_screen_email_wrong_form).tr(),
          ),
        );
      }
    } else {
      setState(() {
        _isLoading = false;
      });
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.red,
          content: Text(
              //Gerekli yerler text
              LocaleKeys.login_screen_fill_the_parts).tr(),
        ),
      );
    }
  }

  // bool validateAndSave() {
  //   final form = _formKey.currentState;
  //   if (form == null) {
  //     throw Exception('form is null');
  //   } else {
  //     if (form.validate()) {
  //       form.save();
  //       print('form is valid');
  //       return true;
  //     }
  //     return false;
  //   }
  // }
}
