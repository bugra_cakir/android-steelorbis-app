// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader{
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>> load(String fullPath, Locale locale ) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String,dynamic> en = {
  "contact_us_title": "Contact Us",
  "contact_us_info": "If you have problems logging into the application, you can contact us via e-mail or phone. While we offer a limited number of services in the mobile application, you can visit our website to see all our services.",
  "login_screen_member_login": "Member Login",
  "login_screen_already_loggedin_in_different_device": "The session is open on a different device. Please log out or contact.",
  "login_screen_invalid_session": "Invalid email or password.",
  "login_screen_logged_in": "Signed in.",
  "login_screen_fill_the_parts": "Fill in the required fields.",
  "login_screen_login_button": "Login",
  "login_screen_password_hint_text": "Password",
  "login_screen_email_hint_text": "Email",
  "login_screen_email_wrong_form": "Email is in wrong form",
  "tabs_screen_menu_home": "Home Page",
  "tabs_screen_analyzes": "Analyzes",
  "tabs_screen_prices": "Prices",
  "tabs_screen_search": "Search",
  "tabs_screen_settings": "Settings"
};
static const Map<String,dynamic> it = {
  "contact_us_title": "Contattaci",
  "contact_us_info": "Se hai problemi ad accedere all'applicazione, puoi contattarci via e-mail o telefono. Sebbene offriamo un numero limitato di servizi nell'applicazione mobile, puoi visitare il nostro sito Web per vedere tutti i nostri servizi.",
  "login_screen_member_login": "Accesso Membri",
  "login_screen_already_loggedin_in_different_device": "La sessione è aperta su un altro dispositivo. Si prega di disconnettersi o contattare.",
  "login_screen_invalid_session": "E-mail o password non validi.",
  "login_screen_logged_in": "Registrato in.",
  "login_screen_fill_the_parts": "Compila i campi richiesti.",
  "login_screen_login_button": "Login ",
  "login_screen_password_hint_text": "Parola d'ordine",
  "login_screen_email_hint_text": "E-mail ",
  "login_screen_email_wrong_form": "L'e-mail è nella forma sbagliata",
  "tabs_screen_menu_home": "Home Page ",
  "tabs_screen_analyzes": "Analisi",
  "tabs_screen_prices": "Prezzi",
  "tabs_screen_search": "Ricerca",
  "tabs_screen_settings": "Impostazioni"
};
static const Map<String,dynamic> tr = {
  "contact_us_title": "Bizle İletişime Geçin",
  "contact_us_info": "Uygulamaya girişte sorun yaşıyorsanız bizimle e-posta veya telefon yoluyla iletişime geçebilirsiniz. Mobil uygulamada sınırlı sayıda hizmet sunarken, tüm hizmetlerimizi görmek için web sitemizi ziyaret edebilirsiniz.",
  "login_screen_member_login": "Üye Girişi",
  "login_screen_already_loggedin_in_different_device": "Oturum farklı bir cihazda açık. Lütfen çıkış yapın veya iletişime geçin.",
  "login_screen_invalid_session": "Geçersiz email veya şifre.",
  "login_screen_logged_in": "Giriş yapıldı.",
  "login_screen_fill_the_parts": "Gerekli yerleri doldurunuz.",
  "login_screen_login_button": "Giriş Yap",
  "login_screen_password_hint_text": "Şifre",
  "login_screen_email_hint_text": "Email ",
  "login_screen_email_wrong_form": "E-posta yanlış biçimde",
  "tabs_screen_menu_home": "Ana Sayfa",
  "tabs_screen_analyzes": "Analizler",
  "tabs_screen_prices": "Fiyatlar",
  "tabs_screen_search": "Ara",
  "tabs_screen_settings": "Ayarlar"
};
static const Map<String,dynamic> zh = {
  "contact_us_title": "聯繫我們",
  "contact_us_info": "如果您在登录应用程序时遇到问题，可以通过电子邮件或电话与我们联系。 虽然我们在移动应用程序中提供的服务数量有限，但您可以访问我们的网站查看我们的所有服务。",
  "login_screen_member_login": "会员登录",
  "login_screen_already_loggedin_in_different_device": "会话已在其他设备上打开 请注销或联系。",
  "login_screen_invalid_session": "无效的电子邮件或密码。",
  "login_screen_logged_in": "已登录。",
  "login_screen_fill_the_parts": "填写必填字段。",
  "login_screen_login_button": "登录",
  "login_screen_password_hint_text": "密码",
  "login_screen_email_hint_text": "电子邮件",
  "login_screen_email_wrong_form": "电子邮件格式错误",
  "tabs_screen_menu_home": "主页",
  "tabs_screen_analyzes": "分析",
  "tabs_screen_prices": "价格",
  "tabs_screen_search": "搜索",
  "tabs_screen_settings": "设置"
};
static const Map<String, Map<String,dynamic>> mapLocales = {"en": en, "it": it, "tr": tr, "zh": zh};
}
