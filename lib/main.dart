import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:steelorbis_android_app/api/login_check_api.dart';
import 'package:steelorbis_android_app/api/sendtoken_api.dart';
import 'package:steelorbis_android_app/translations/codegen_loader.g.dart';

import 'Screens/login_screen.dart';
import 'Screens/tabs_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //EasyLocalization package entegration for multi-language
  await EasyLocalization.ensureInitialized();
  await Firebase.initializeApp();

  late final FirebaseMessaging firebaseMessaging;
  firebaseMessaging = FirebaseMessaging.instance;
  var FBtoken = await firebaseMessaging.getToken();

  //send some data
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  SharedPreferences FBTokenPreferences = await SharedPreferences.getInstance();

  FBTokenPreferences.setString('FirebaseToken', FBtoken!);
  print("Instance ID on main: $FBtoken");

  //TokenService Call at the begining
  sendTokenService(FBtoken);
  bool? isLoggedIn = false;

  String? ticket = sharedPreferences.getString('token');
  String? username = sharedPreferences.getString('email');
  print('username on main: $username');
  print('ticket on main: $ticket');

  if (ticket == null || username == null) {
    print('ticket or username is null');
  } else {
    isLoggedIn = (await checkLoginService(username, ticket));
  }
  print('what is isLoggedIn? at main: ${isLoggedIn.toString()}');

  runApp(
    //Multi Language support
    EasyLocalization(
      supportedLocales: [
        Locale('en'),
        Locale('tr'),
        Locale('zh'),
        Locale('it'),
      ],
      path: 'assets/translations',

      fallbackLocale:
          Locale('en'), //If can not find the supportedLocales return to English
      assetLoader: CodegenLoader(),
      child: AndroidAppSteelOrbis(isLoggedIn: isLoggedIn),
    ),
  );
}

class AndroidAppSteelOrbis extends StatefulWidget {
  final bool? isLoggedIn;
  const AndroidAppSteelOrbis({Key? key, required this.isLoggedIn})
      : super(key: key);
  @override
  State<AndroidAppSteelOrbis> createState() => _AndroidAppSteelOrbisState();
}

class _AndroidAppSteelOrbisState extends State<AndroidAppSteelOrbis> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    // SharedPreferences? _preferences;
    // bool? isLoggedInMain = _preferences?.getBool('isLoggedIn');
    // print('isLoggedInMain come from main to MaterialApp is: $isLoggedInMain');

    return MaterialApp(
      //remove banner
      debugShowCheckedModeBanner: false,

      //define the locales(multilanguage) for context
      supportedLocales: context.supportedLocales,
      localizationsDelegates: context.localizationDelegates,
      locale: context.locale,
      home: widget.isLoggedIn == true ? TabsScreen() : LoginScreen(),
      theme: ThemeData(
        backgroundColor: Colors.grey,
        secondaryHeaderColor: Colors.blueAccent,
        primarySwatch: Colors.grey,
      ),
    );
  }

  // static Future init() async {
  //   SharedPreferences _preferences = await SharedPreferences.getInstance();
  // }
}
