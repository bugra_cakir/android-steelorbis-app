import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

import 'line_titles.dart';

class LineChartWidget extends StatelessWidget {
  final List<Color> gradientColor = [
    const Color(0xff0d47a1),
    const Color(0xff3b3bbf),
  ];
  final List<Color> gradientAboveColors = [
    const Color(0xff23b6e6),
    const Color(0xff02d39a),
  ];
  @override
  Widget build(BuildContext context) {
    return LineChart(
      LineChartData(
        axisTitleData: FlAxisTitleData(
          leftTitle: AxisTitle(
            showTitle: true,
            titleText: 'Price',
            margin: 0,
          ),
          bottomTitle: AxisTitle(
              showTitle: true,
              margin: 0,
              titleText: 'Week',
              textAlign: TextAlign.center),
        ),
        titlesData: LineTitles.getTitleData(),
        minX: 0,
        maxX: 52,
        minY: 0,
        maxY: 8,
        gridData: FlGridData(
          show: true,
          drawHorizontalLine: true,
          drawVerticalLine: true,
          getDrawingHorizontalLine: (value) {
            return FlLine(
              color: const Color(0xffA7a4a4),
              strokeWidth: 1,
            );
          },
          getDrawingVerticalLine: (value) {
            return FlLine(
              color: const Color(0xffA7a4a4),
              strokeWidth: 0.5,
            );
          },
        ),
        borderData:
            FlBorderData(show: true, border: Border.all(color: Colors.red)),
        lineBarsData: [
          LineChartBarData(
            spots: [
              FlSpot(0, 2.68), //week 1
              FlSpot(1, 2.75),
              FlSpot(2, 2.77),
              FlSpot(3, 2.89),
              FlSpot(4, 3.11),
              FlSpot(5, 3.19),
              FlSpot(6, 3.09),
              FlSpot(7, 3.22),
              FlSpot(8, 3.57),
              FlSpot(9, 3.65),
              FlSpot(10, 3.86),
              FlSpot(11, 4),
              FlSpot(23, 3.10),
              FlSpot(25, 3.8),
              FlSpot(27, 4.12),
              FlSpot(31, 4.78),
              FlSpot(33, 5.12),
              FlSpot(38, 5.78),
              FlSpot(42, 6.22),
              FlSpot(46, 6.88),
              FlSpot(50, 7.12),
              FlSpot(51, 7.88),
              FlSpot(52, 8),
            ],
            isCurved: true,
            colors: gradientColor,
            barWidth: 4,
            belowBarData: BarAreaData(
              show: true,
              colors: gradientAboveColors
                  .map((color) => color.withOpacity(0.8))
                  .toList(),
            ),
            // dotData: FlDotData(show: false,),//showing dots
          ),
        ],
      ),
    );
  }
}
