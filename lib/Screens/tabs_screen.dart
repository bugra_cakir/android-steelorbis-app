import 'dart:ui';

import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:steelorbis_android_app/Screens/carousel_with_dots_page.dart';
import 'package:steelorbis_android_app/Screens/login_screen.dart';
import 'package:steelorbis_android_app/Widgets/line_chart_widget.dart';
import 'package:steelorbis_android_app/api/logout_api.dart';
import 'package:steelorbis_android_app/translations/locale_keys.g.dart';

class TabsScreen extends StatefulWidget {
  String? username;
  TabsScreen({this.username});
  @override
  State<TabsScreen> createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  int currentIndexOfTab = 0;
  final List<String> imgDummy = [
    'https://www.pilliweb.com/wp-content/uploads/2019/10/vasifli-celikler.jpg',
    'https://d1hfln2sfez66z.cloudfront.net/03-18-2021/t_12809f90ba184c0498c57705345dd05b_name_whbq_live.png',
    'https://servicepath.co/wp-content/uploads/2019/11/news.jpg',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQuDwuV4LUgKY4g0p3KG-yP9nYyj_z-ZuOlxw&usqp=CAU',
  ];

  String get username => widget.username.toString();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          actions: [
            IconButton(
              icon: Icon(Icons.logout),
              onPressed: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.remove('token');
                print('The passed username is at mainPage: ${widget.username}');
                logoutService(username);

                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext ctx) => LoginScreen()));
                print('LOGGED OUT: ${widget.username}');
              },
            ),
          ],
          title: Center(
            child: Image.asset(
              'assets/icons/logo_buyuk.png',
              height: 150,
              width: 180,
            ),
          ),
          backgroundColor: Colors.black54,
          shadowColor: Colors.grey,
        ),
        // Body Part
        body: SafeArea(
          child: Center(
            child: SingleChildScrollView(
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        const Padding(
                          padding: EdgeInsets.all(5),
                        ),
                        CarouselWithDotsPage(imgDummy),
                        SizedBox(
                          width: 350,
                          child: Card(
                            color: Colors.white38,
                            child: Text(
                              'Günlük Çelik Fiyatları',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 22,
                              ),
                            ),
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              width: 350,
                              height: 400,
                              child: ListView.builder(
                                itemCount: 3,
                                itemBuilder: (contex, index) {
                                  return Card(
                                    child: Column(
                                      children: [
                                        ListTile(
                                          leading: Text(
                                            'Düşük - Yüksek',
                                            textAlign: TextAlign.left,
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.white),
                                          ),
                                          trailing: Text(
                                            'Token:  ',
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.white),
                                          ),
                                          title: Text(
                                            'Ortalama',
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.white),
                                            textAlign: TextAlign.center,
                                          ),
                                          tileColor: Colors.blueAccent,
                                        ),
                                        Image.network(
                                            'https://www.esnmetal.com/wp-content/uploads/2019/10/galvaniz-boru-fiyatlari.jpg'),
                                      ],
                                    ),
                                  );
                                },
                              ),
                            ),
                            const SizedBox(height: 20),
                            Container(
                              decoration: BoxDecoration(
                                color: const Color(0xffDfd8d8),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              margin: const EdgeInsets.all(12),
                              width: 350,
                              height: 400,
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(0, 20, 12, 0),
                                child: Column(
                                  children: [
                                    const Text(
                                      'Haftalık Çelik Fiyatları',
                                      style: TextStyle(
                                        fontSize: 15,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    Expanded(child: LineChartWidget()),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        bottomNavigationBar: buildBottomNavigationBar(context),
      ),
    );
  }

  BottomNavigationBar buildBottomNavigationBar(BuildContext context) {
    return BottomNavigationBar(
      selectedItemColor: Colors.deepOrange,
      type: BottomNavigationBarType.shifting,
      unselectedItemColor: Colors.black54,
      showUnselectedLabels: true,
      selectedFontSize: 12,
      unselectedFontSize: 12,
      onTap: (index) => setState(() => currentIndexOfTab = index),
      currentIndex: currentIndexOfTab,
      items: [
        BottomNavigationBarItem(
          label: LocaleKeys.tabs_screen_menu_home.tr(),
          icon: Icon(
            Icons.home_outlined,
          ),
        ),
        BottomNavigationBarItem(
          label: LocaleKeys.tabs_screen_analyzes.tr(),
          icon: Icon(
            FontAwesomeIcons.chartLine,
          ),
        ),
        BottomNavigationBarItem(
          label: LocaleKeys.tabs_screen_prices.tr(),
          icon: Icon(
            FontAwesomeIcons.handHoldingUsd,
          ),
        ),
        BottomNavigationBarItem(
          label: LocaleKeys.tabs_screen_search.tr(),
          icon: Icon(
            FontAwesomeIcons.search,
          ),
        ),
        BottomNavigationBarItem(
          label: LocaleKeys.tabs_screen_settings.tr(),
          icon: Icon(
            FontAwesomeIcons.cog,
          ),
        ),
      ],
    );
  }
}
